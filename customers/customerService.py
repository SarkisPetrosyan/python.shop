from base.BaseService import BaseService
from .customerRepository import CustomerRepository
from .customerModel import Customer


class CustomerService(BaseService):

    def __init__(self):
        super().__init__()
        self.repository = CustomerRepository()

    def add_customer(self, customer):
        try:
            self.repository.add_customer(customer)
        except Exception as e:
            print(e, self.error_message)

    def edit_customer(self, changed_customer, customer_id):
        try:
            customer = self.get_customer_by_id(customer_id)
            if customer is not None:
                customer.name = changed_customer.name
                customer.address = changed_customer.address
                customer.city = changed_customer.city
                customer.country = changed_customer.country
                customer.postalCode = changed_customer.postalCode
                self.repository.edit_entity(customer, customer_id)
        except Exception as e:
            print(e, self.error_message)

    def delete_customer(self, customer_id):
        try:
            customer = self.get_customer_by_id(customer_id)
            if customer is not None:
                self.repository.delete_entity(customer_id)
        except Exception as e:
            print(e, self.error_message)

    def get_customers(self):
        db_result = self.repository.get_all()
        customers = []
        for db_customer in db_result:
            customer = Customer(db_customer[1], db_customer[2], db_customer[3], db_customer[4], db_customer[5])
            customer.id = db_customer[0]
            customers.append(customer)
        return customers

    def get_customer_by_id(self, customer_id):
        try:
            db_result = self.repository.get_entity(customer_id)
            customer = Customer(db_result[1], db_result[2], db_result[3], db_result[4], db_result[5])
            return customer
        except Exception as e:
            print(e, self.error_message)

    def get_customer_max_id(self):
        try:
            db_result = self.repository.get_entity_max_id()
            return db_result
        except Exception as e:
            print(e, self.error_message)

    def get_customers_count(self):
        try:
            db_result = self.repository.get_entity_count()
            return db_result
        except Exception as e:
            print(e, self.error_message)
