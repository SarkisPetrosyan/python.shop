import unittest
from faker import Faker
from customers.customerModel import Customer
from customers.customerService import CustomerService
import random


class TestCustomer(unittest.TestCase):

    def test_addCustomer(self):
        try:
            customer_service = CustomerService()
            fake = Faker()
            for i in range(0, 20):
                customer = Customer(fake.name(), fake.address(), fake.city(), fake.postcode(), fake.country())
                customer_service.add_customer(customer)
        except Exception as e:
            self.fail(e)

    def test_editCustomer(self):
        try:
            customer_service = CustomerService()
            fake = Faker('ru_RU')
            customers = customer_service.get_customers()
            for customer in customers:
                old_count = customer_service.get_customers_count()
                customer.name = fake.name()
                customer.address = fake.address()
                customer.city = fake.city()
                customer.postalCode = fake.postcode()
                customer.country = fake.country()
                customer_service.edit_customer(customer, customer.id)

                new_count = customer_service.get_customers_count()
                edited_customer = customer_service.get_customer_by_id(customer.id)

                self.assertEqual(old_count, new_count, "Customer count should be equal")
                self.assertEqual(customer.name, edited_customer.name)

        except Exception as e:
            self.fail(e)

    def test_getCustomerById(self):
        try:
            customer_service = CustomerService()
            max_id = customer_service.get_customer_max_id()
            customer_service.get_customer_by_id(random.randint(1, max_id))
        except Exception as e:
            self.fail(e)

    def test_deleteCustomerById(self):
        try:
            customer_service = CustomerService()
            max_id = customer_service.get_customer_max_id()
            customer_service.delete_customer(random.randint(1, max_id))
        except Exception as e:
            self.fail(e)

    def test_customer_max_id(self):
        try:
            customer_service = CustomerService()
            customer_max_id = customer_service.get_customer_max_id()
            self.assertGreaterEqual(customer_max_id, 0, "Id should be bigger than 0")
        except Exception as e:
            self.fail(e)

    def test_customer_count(self):
        try:
            customer_service = CustomerService()
            customers_count = customer_service.get_customers_count()
            self.assertGreaterEqual(customers_count, 0, "Id should be bigger than 0")
        except Exception as e:
            self.fail(e)
