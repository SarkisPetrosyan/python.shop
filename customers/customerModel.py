from base.BaseModel import Base


class Customer(Base):
    def __init__(self, name, address, city, postal_code, country):
        self.name = name
        self.address = address
        self.city = city
        self.postalCode = postal_code
        self.country = country
        self.id = 0
