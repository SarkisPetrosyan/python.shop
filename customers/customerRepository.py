from mysql.connector import Error
from base.BaseRepository import BaseRepository


class CustomerRepository(BaseRepository):

    def __init__(self):
        super().__init__()
        self.table = "customers"
        self.values = '(%s, %s, %s, %s, %s)'
        self.create_table_sql = '(id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), address VARCHAR(255), ' \
                                'city VARCHAR(255), postalCode VARCHAR(255), country VARCHAR(255)) '

    def add_customer(self, customer):
        try:
            if not self.db_connector.check_is_table_exists(self.table):
                sql = f"CREATE TABLE {self.table} {self.create_table_sql}"
                self.db_connector.command_executor(sql)

            self.add_entity(customer)
        except Error as e:
            print(e)
