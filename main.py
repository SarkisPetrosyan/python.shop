import unittest

from categories.test_category import TestCategory
from customers.test_customer import TestCustomer
from dbconnector.test_db import TestDatabase
from employees.test_employee import TestEmployee
from products.test_product import TestProduct
from shipper.test_shipper import TestShipper
from suppliers.test_supplier import TestSupplier

if __name__ == "__main__":
    calcTestSuite = unittest.TestSuite()
    calcTestSuite.addTest(unittest.makeSuite(TestDatabase))
    calcTestSuite.addTest(unittest.makeSuite(TestCustomer))
    calcTestSuite.addTest(unittest.makeSuite(TestCategory))
    calcTestSuite.addTest(unittest.makeSuite(TestProduct))
    calcTestSuite.addTest(unittest.makeSuite(TestEmployee))
    calcTestSuite.addTest(unittest.makeSuite(TestShipper))
    calcTestSuite.addTest(unittest.makeSuite(TestSupplier))
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(calcTestSuite)
