from base.BaseModel import Base


class Product(Base):
    def __init__(self, name, category_id, unit, price):
        self.name = name
        self.category_id = category_id
        self.unit = unit
        self.price = price
        self.id = 0
