from base.BaseService import BaseService
from products.productRepository import ProductRepository


class ProductService(BaseService):

    def __init__(self):
        super().__init__()
        self.repository = ProductRepository()

    def add_product(self, product):
        try:
            self.repository.add_product(product)
        except Exception as e:
            print(e, self.error_message)
