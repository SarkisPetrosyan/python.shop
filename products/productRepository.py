from mysql.connector import Error
from base.BaseRepository import BaseRepository


class ProductRepository(BaseRepository):

    def __init__(self):
        super().__init__()
        self.table = "products"
        self.values = '(%s, %s, %s, %s)'
        self.create_table_sql = '(id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), category_id INT, ' \
                                'unit VARCHAR(255), price INT,' \
                                'CONSTRAINT FK_CategoryProduct FOREIGN KEY (category_id) REFERENCES categories(' \
                                'id) ON UPDATE CASCADE ON DELETE CASCADE)'

    def add_product(self, product):
        try:
            if not self.db_connector.check_is_table_exists(self.table):
                sql = f"CREATE TABLE {self.table} {self.create_table_sql}"
                self.db_connector.command_executor(sql)

            self.add_entity(product)
        except Error as e:
            print(e)
