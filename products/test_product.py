import unittest
from faker import Faker

from categories.categoryService import CategoryService
from products.productModel import Product
from products.productService import ProductService
import random


class TestProduct(unittest.TestCase):

    def test_addProduct(self):
        try:
            category_service = CategoryService()
            max_id = category_service.get_entity_max_id()
            product_service = ProductService()
            fake = Faker()
            for i in range(0, 20):
                category = category_service.get_category_by_id(max_id)

                if category is not None:
                    product = Product(fake.name(), category.id, 'KG', 1500)
                    product_service.add_product(product)
        except Exception as e:
            print(e)
