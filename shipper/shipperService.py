from base.BaseService import BaseService
from .shipperRepository import ShipperRepository
from .shipperModel import Shipper


class ShipperService(BaseService):

    def __init__(self):
        super().__init__()
        self.repository = ShipperRepository()

    def add_shipper(self, shipper):
        try:
            self.repository.add_shipper(shipper)
        except Exception as e:
            print(e, self.error_message)

    def edit_shipper(self, changed_shipper, shipper_id):
        try:
            shipper = self.get_shipper_by_id(shipper_id)
            if shipper is not None:
                shipper.name = changed_shipper.name
                shipper.phone = changed_shipper.phone
                self.repository.edit_entity(shipper, shipper_id)
        except Exception as e:
            print(e, self.error_message)

    def delete_shipper(self, shipper_id):
        try:
            shipper = self.get_shipper_by_id(shipper_id)
            if shipper is not None:
                self.repository.delete_entity(shipper_id)
        except Exception as e:
            print(e, self.error_message)

    def get_shippers(self):
        db_result = self.repository.get_all()
        shippers = []
        for db_shipper in db_result:
            shipper = Shipper(db_shipper[1], db_shipper[2])
            shipper.id = db_shipper[0]
            shippers.append(shipper)
        return shippers

    def get_shipper_by_id(self, shipper_id):
        try:
            db_result = self.repository.get_entity(shipper_id)
            shipper = Shipper(db_result[1], db_result[2])
            return shipper
        except Exception as e:
            print(e, self.error_message)

    def get_shipper_max_id(self):
        try:
            db_result = self.repository.get_entity_max_id()
            return db_result
        except Exception as e:
            print(e, self.error_message)

    def get_shipper_count(self):
        try:
            db_result = self.repository.get_entity_count()
            return db_result
        except Exception as e:
            print(e, self.error_message)
