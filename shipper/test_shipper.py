import unittest
from faker import Faker
import random

from shipper.shipperModel import Shipper
from shipper.shipperService import ShipperService


class TestShipper(unittest.TestCase):

    def test_addShipper(self):
        try:
            shipper_service = ShipperService()
            fake = Faker()
            for i in range(0, 20):
                shipper = Shipper(fake.name(), fake.phone_number())
                shipper_service.add_shipper(shipper)
        except Exception as e:
            self.fail(e)

    def test_editShipper(self):
        try:
            shipper_service = ShipperService()
            fake = Faker('ru_RU')
            shippers = shipper_service.get_shippers()
            for shipper in shippers:
                old_count = shipper_service.get_shipper_count()
                shipper.name = fake.name()
                shipper.phone = fake.phone_number()
                shipper_service.edit_shipper(shipper, shipper.id)

                new_count = shipper_service.get_shipper_count()
                edited_shipper = shipper_service.get_shipper_by_id(shipper.id)

                self.assertEqual(old_count, new_count, "Shipper count should be equal")
                self.assertEqual(shipper.name, edited_shipper.name)

        except Exception as e:
            self.fail(e)

    def test_getShipperById(self):
        try:
            shipper_service = ShipperService()
            max_id = shipper_service.get_shipper_max_id()
            shipper_service.get_shipper_by_id(random.randint(1, max_id))
        except Exception as e:
            self.fail(e)

    def test_deleteShipperById(self):
        try:
            shipper_service = ShipperService()
            max_id = shipper_service.get_shipper_max_id()
            shipper_service.delete_shipper(random.randint(1, max_id))
        except Exception as e:
            self.fail(e)

    def test_shipper_max_id(self):
        try:
            shipper_service = ShipperService()
            shipper_max_id = shipper_service.get_shipper_max_id()
            self.assertGreaterEqual(shipper_max_id, 0, "Id should be bigger than 0")
        except Exception as e:
            self.fail(e)

    def test_shipper_count(self):
        try:
            shipper_service = ShipperService()
            shipper_count = shipper_service.get_shipper_count()
            self.assertGreaterEqual(shipper_count, 0, "Id should be bigger than 0")
        except Exception as e:
            self.fail(e)
