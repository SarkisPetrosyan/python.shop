from base.BaseModel import Base


class Shipper(Base):
    def __init__(self, name, phone):
        self.name = name
        self.phone = phone
        self.id = 0