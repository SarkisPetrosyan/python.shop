from base.BaseRepository import BaseRepository
from mysql.connector import Error


class ShipperRepository(BaseRepository):

    def __init__(self):
        super().__init__()
        self.table = "shippers"
        self.values = '(%s, %s)'
        self.create_table_sql = '(id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), phone VARCHAR(255))'

    def add_shipper(self, product):
        try:
            if not self.db_connector.check_is_table_exists(self.table):
                sql = f"CREATE TABLE {self.table} {self.create_table_sql}"
                self.db_connector.command_executor(sql)

            self.add_entity(product)
        except Error as e:
            print(e)
