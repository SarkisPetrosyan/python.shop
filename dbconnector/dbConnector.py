from mysql.connector import Error
import mysql


class DbConnector:

    def __init__(self):
        self.host = "localhost"
        self.user = "root"
        self.password = "admin"
        self.db = "shop"

    def db_connect(self):
        try:
            db = mysql.connector.connect(host=self.host,
                                         user=self.user,
                                         passwd=self.password,
                                         db=self.db)
            return db
        except Error as e:
            print(e)

    def db_cursor(self):
        db = self.db_connect()
        return db.cursor()

    def check_is_table_exists(self, table):
        db = self.db_connect()
        cursor = db.cursor()
        query = f"SHOW TABLES LIKE '{table}';"
        cursor.execute(query)
        result = list(cursor.fetchall())
        return len(result) > 0

    def command_executor(self, sql):
        db = self.db_connect()
        cursor = db.cursor()
        cursor.execute(sql)

    def get_all_tables(self):
        db = self.db_connect()
        cursor = db.cursor()
        cursor.execute("SELECT table_name FROM information_schema.tables WHERE table_schema = 'shop'")
        tables = list(cursor.fetchall())
        tables_list = []
        for table in tables:
            tables_list.append(table[0])
        return tables_list
