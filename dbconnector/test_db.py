import unittest

from dbconnector.dbConnector import DbConnector


class TestDatabase(unittest.TestCase):

    def test_truncateTable(self):
        try:
            db_connector = DbConnector()
            tables = db_connector.get_all_tables()
            for table in tables:
                sql = f'TRUNCATE table {table}'
                db_connector.command_executor(sql)

        except Exception as e:
            self.fail(e)
