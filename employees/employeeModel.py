from base.BaseModel import Base


class Employee(Base):
    def __init__(self, lastname, firstname, birth_date, photo, notes):
        self.lastname = lastname
        self.firstname = firstname
        self.birthDate = birth_date
        self.photo = photo
        self.notes = notes
        self.id = 0
