import unittest
from faker import Faker
from employees.employeeModel import Employee
from employees.employeeService import EmployeeService
import random


class TestEmployee(unittest.TestCase):

    def test_addEmployee(self):
        try:
            employee_service = EmployeeService()
            fake = Faker()
            for i in range(0, 20):
                employee = Employee(fake.last_name(), fake.first_name(), fake.date(), fake.image_url(), fake.text())
                employee_service.add_employee(employee)
        except Exception as e:
            self.fail(e)

    def test_editEmployee(self):
        try:
            employee_service = EmployeeService()
            fake = Faker('ru_RU')
            employees = employee_service.get_employees()
            for employee in employees:
                old_count = employee_service.get_count()
                employee.lastname = fake.last_name()
                employee.firstname = fake.first_name()
                employee.birthDate = fake.date()
                employee.photo = fake.image_url()
                employee.notes = fake.text()

                employee_service.edit_employee(employee, employee.id)

                new_count = employee_service.get_count()
                edited_employee = employee_service.get_employee_by_id(employee.id)

                self.assertEqual(old_count, new_count, "Employee count should be equal")
                self.assertEqual(employee.firstname, edited_employee.firstname)
                self.assertEqual(employee.lastname, edited_employee.lastname)

        except Exception as e:
            self.fail(e)

    def test_getEmployeeById(self):
        try:
            employee_service = EmployeeService()
            max_id = employee_service.get_entity_max_id()
            employee_service.get_employee_by_id(random.randint(1, max_id))
        except Exception as e:
            self.fail(e)

    def test_deleteEmployeeById(self):
        try:
            employee_service = EmployeeService()
            max_id = employee_service.get_entity_max_id()
            employee_service.delete_employee(random.randint(1, max_id))
        except Exception as e:
            self.fail(e)

    def test_employee_max_id(self):
        try:
            employee_service = EmployeeService()
            employee_max_id = employee_service.get_entity_max_id()
            self.assertGreaterEqual(employee_max_id, 0, "Id should be bigger than 0")
        except Exception as e:
            self.fail(e)

    def test_employee_count(self):
        try:
            employee_service = EmployeeService()
            employees_count = employee_service.get_count()
            self.assertGreaterEqual(employees_count, 0, "Id should be bigger than 0")
        except Exception as e:
            self.fail(e)
