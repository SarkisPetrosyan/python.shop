from base.BaseService import BaseService
from .employeeRepository import EmployeeRepository
from .employeeModel import Employee


class EmployeeService(BaseService):

    def __init__(self):
        super().__init__()
        self.repository = EmployeeRepository()

    def add_employee(self, employee):
        try:
            self.repository.add_employee(employee)
        except Exception as e:
            print(e, self.error_message)

    def edit_employee(self, changed_employee, employee_id):
        try:
            employee = self.get_employee_by_id(employee_id)
            if employee is not None:
                employee.firstname = changed_employee.firstname
                employee.lastname = changed_employee.lastname
                self.repository.edit_entity(employee, employee_id)
        except Exception as e:
            print(e, self.error_message)

    def delete_employee(self, employee_id):
        try:
            employee = self.get_employee_by_id(employee_id)
            if employee is not None:
                self.repository.delete_entity(employee_id)
        except Exception as e:
            print(e, self.error_message)

    def get_employees(self):
        db_result = self.repository.get_all()
        employees = []
        for db_employee in db_result:
            employee = Employee(db_employee[1], db_employee[2], db_employee[3], db_employee[4], db_employee[5])
            employee.id = db_employee[0]
            employees.append(employee)
        return employees

    def get_employee_by_id(self, employee_id):
        try:
            db_result = self.repository.get_entity(employee_id)
            employee = Employee(db_result[1], db_result[2], db_result[3], db_result[4], db_result[5])
            return employee
        except Exception as e:
            print(e, self.error_message)
