from mysql.connector import Error
from base.BaseRepository import BaseRepository


class EmployeeRepository(BaseRepository):

    def __init__(self):
        super().__init__()
        self.table = "employees"
        self.values = '(%s, %s, %s, %s, %s)'
        self.create_table_sql = '(id INT AUTO_INCREMENT PRIMARY KEY, lastName VARCHAR(255), firstName VARCHAR(255), ' \
                                'birthDate VARCHAR(255), photo VARCHAR(255), notes VARCHAR(255)) '

    def add_employee(self, employee):
        try:
            if not self.db_connector.check_is_table_exists(self.table):
                sql = f"CREATE TABLE {self.table} {self.create_table_sql}"
                self.db_connector.command_executor(sql)

            self.add_entity(employee)
        except Error as e:
            print(e)
