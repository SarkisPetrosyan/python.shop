from base.BaseRepository import BaseRepository
from mysql.connector import Error


class SupplierRepository(BaseRepository):
    def __init__(self):
        super().__init__()
        self.table = 'suppliers'
        self.values = '(%s, %s, %s, %s, %s, %s, %s)'
        self.create_table = '(id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), contact VARCHAR(255), ' \
                            'address VARCHAR(255), city VARCHAR(255), postcode VARCHAR(255), country VARCHAR(255),' \
                            'phone VARCHAR(255))'

    def add_supplier(self, supplier):
        try:
            if not self.db_connector.check_is_table_exists(self.table):
                sql = f"CREATE TABLE {self.table} {self.create_table}"
                self.db_connector.command_executor(sql)

            self.add_entity(supplier)
        except Error as e:
            print('Something went wrong... Error:', e)
