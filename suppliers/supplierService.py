from base.BaseService import BaseService
from .supplierRepository import SupplierRepository
from .supplierModel import Supplier


class SupplierService(BaseService):
    def __init__(self):
        super().__init__()
        self.repository = SupplierRepository()

    def add_supplier(self, supplier):
        try:
            self.repository.add_supplier(supplier)
        except Exception as e:
            print(e, self.error_message)

    def edit_supplier(self, changed_supplier, supplier_id):
        try:
            supplier = self.get_supplier_by_id(supplier_id)
            if supplier is not None:
                supplier.name = changed_supplier.name
                supplier.contact = changed_supplier.contact
                supplier.address = changed_supplier.address
                supplier.city = changed_supplier.city
                supplier.index = changed_supplier.index
                supplier.country = changed_supplier.country
                supplier.phone = changed_supplier.phone
                self.repository.edit_entity(supplier, supplier_id)
        except Exception as e:
            print(e, self.error_message)

    def delete_supplier(self, supplier_id):
        try:
            supplier = self.get_supplier_by_id(supplier_id)
            if supplier is not None:
                self.repository.delete_entity(supplier_id)
        except Exception as e:
            print(e, self.error_message)

    def get_suppliers(self):
        db_result = self.repository.get_all()
        suppliers = []
        for db_supplier in db_result:
            supplier = Supplier(db_supplier[1], db_supplier[2], db_supplier[3], db_supplier[4], db_supplier[5],
                                db_supplier[6], db_supplier[7])
            supplier.id = db_supplier[0]
            suppliers.append(supplier)
        return suppliers

    def get_supplier_by_id(self, supplier_id):
        try:
            db_result = self.repository.get_entity(supplier_id)
            supplier = Supplier(db_result[1], db_result[2], db_result[3], db_result[4], db_result[5], db_result[6],
                                db_result[7])
            return supplier
        except Exception as e:
            print(e, self.error_message)

    def get_suppliers_count(self):
        try:
            db_result = self.repository.get_entity_count()
            return db_result
        except Exception as e:
            print(e, self.error_message)

    def get_supplier_max_id(self):
        try:
            db_result = self.repository.get_entity_max_id()
            return db_result
        except Exception as e:
            print(e, self.error_message)
