from base.BaseModel import Base


class Supplier(Base):
    def __init__(self, name, contact, address, city, postal_code, country, phone):
        self.id = 0
        self.name = name
        self.contact = contact
        self.address = address
        self.city = city
        self.postcode = postal_code
        self.country = country
        self.phone = phone
