import unittest
from faker import Faker
from suppliers.supplierModel import Supplier
from suppliers.supplierService import SupplierService
import random


class TestSupplier(unittest.TestCase):

    def test_add_supplier(self):
        try:
            supplier_service = SupplierService()
            fake = Faker('ru_RU')
            for i in range(0, 20):
                supplier = Supplier(fake.name(), fake.name(), fake.address(), fake.city(), fake.postcode(),
                                    fake.country(), fake.phone_number())
                supplier_service.add_supplier(supplier)
        except Exception as e:
            self.fail(e)

    def test_edit_supplier(self):
        try:
            fake = Faker('ru_RU')
            supplier_service = SupplierService()
            suppliers = supplier_service.get_suppliers()
            for supplier in suppliers:
                old_count = supplier_service.get_suppliers_count()
                supplier.name = fake.name()
                supplier.contact = fake.name()
                supplier.address = fake.address()
                supplier.city = fake.city()
                supplier.postcode = fake.postcode()
                supplier.country = fake.country()
                supplier.phone = fake.phone_number()
                supplier_service.edit_supplier(supplier, supplier.id)
                new_count = supplier_service.get_suppliers_count()
                edited_supplier = supplier_service.get_supplier_by_id(supplier.id)
                self.assertEqual(old_count, new_count, "Supplier count should be equal")
                self.assertEqual(supplier.name, edited_supplier.name)
        except Exception as e:
            self.fail(e)

    def test_delete_supplier_by_id(self):
        try:
            supplier_service = SupplierService()
            max_id = supplier_service.get_supplier_max_id()
            supplier_service.delete_supplier(random.randint(1, max_id))
        except Exception as e:
            self.fail(e)

    def test_get_supplier_by_id(self):
        try:
            supplier_service = SupplierService()
            max_id = supplier_service.get_supplier_max_id()
            supplier_service.get_supplier_by_id(random.randint(1, max_id))
        except Exception as e:
            self.fail(e)

    def test_supplier_count(self):
        try:
            supplier_service = SupplierService()
            suppliers_count = supplier_service.get_suppliers_count()
            self.assertGreaterEqual(suppliers_count, 0, "ID should be bigger than 0.")
        except Exception as e:
            self.fail(e)
