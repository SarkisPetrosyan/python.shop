import unittest
import random

from faker import Faker

from categories.categoryModel import Category
from categories.categoryService import CategoryService


class TestCategory(unittest.TestCase):

    def test_addCategory(self):
        try:
            fake = Faker()
            category_service = CategoryService()
            for i in range(0, 20):
                category = Category(fake.name(), fake.text())
                category_service.add_category(category)
        except Exception as e:
            self.fail(e)

    def test_editCategory(self):
        try:
            category_service = CategoryService()
            fake = Faker('ru_RU')
            categories = category_service.get_categories()
            for category in categories:
                old_count = category_service.get_count()
                category.name = fake.name()
                category.address = fake.address()
                category_service.edit_category(category, category.id)

                new_count = category_service.get_count()
                edited_category = category_service.get_category_by_id(category.id)

                self.assertEqual(old_count, new_count, "Category count should be equal")
                self.assertEqual(category.name, edited_category.name)

        except Exception as e:
            self.fail(e)

    def test_getCategoryById(self):
        try:
            category_service = CategoryService()
            max_id = category_service.get_entity_max_id()
            category_service.get_category_by_id(random.randint(1, max_id))
        except Exception as e:
            self.fail(e)

    def test_deleteCategoryById(self):
        try:
            category_service = CategoryService()
            max_id = category_service.get_entity_max_id()
            category_service.delete_category(random.randint(1, max_id))
        except Exception as e:
            self.fail(e)

    def test_category_max_id(self):
        try:
            category_service = CategoryService()
            category_max_id = category_service.get_entity_max_id()
            self.assertGreaterEqual(category_max_id, 0, "Id should be bigger than 0")
        except Exception as e:
            self.fail(e)

    def test_categories_count(self):
        try:
            category_service = CategoryService()
            categories_count = category_service.get_count()
            self.assertGreaterEqual(categories_count, 0, "Id should be bigger than 0")
        except Exception as e:
            self.fail(e)
