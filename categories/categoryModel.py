from base.BaseModel import Base


class Category(Base):
    def __init__(self, name, description):
        self.name = name
        self.description = description
        self.id = 0
