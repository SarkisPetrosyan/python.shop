from base.BaseRepository import BaseRepository
from mysql.connector import Error


class CategoryRepository(BaseRepository):

    def __init__(self):
        super().__init__()
        self.table = "categories"
        self.values = '(%s, %s)'
        self.create_table_sql = '(id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), description VARCHAR(255))'

    def add_category(self, category):
        try:
            if not self.db_connector.check_is_table_exists(self.table):
                sql = f"CREATE TABLE {self.table} {self.create_table_sql}"
                self.db_connector.command_executor(sql)

            self.add_entity(category)
        except Error as e:
            print(e)

    def edit_category(self, category, category_id):
        self.edit_entity(category, category_id)
