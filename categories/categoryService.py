from base.BaseService import BaseService
from .categoryModel import Category
from .categoryRepository import CategoryRepository


class CategoryService(BaseService):

    def __init__(self):
        super().__init__()
        self.repository = CategoryRepository()

    def add_category(self, category):
        try:
            self.repository.add_category(category)
        except Exception as e:
            print(e, "Something went wrong")

    def get_category_by_id(self, category_id):
        try:
            db_result = self.repository.get_entity(category_id)
            category = Category(db_result[1], db_result[2])
            category.id = category_id
            return category
        except Exception as e:
            print(e, self.error_message)

    def edit_category(self, changed_category, category_id):
        try:
            category = self.get_category_by_id(category_id)
            if category is not None:
                category.name = changed_category.name
                category.description = changed_category.description
                self.repository.edit_entity(category, category_id)
        except Exception as e:
            print(e, self.error_message)

    def delete_category(self, category_id):
        try:
            category = self.get_category_by_id(category_id)
            if category is not None:
                self.repository.delete_entity(category_id)
        except Exception as e:
            print(e, self.error_message)

    def get_categories(self):
        db_result = self.repository.get_all()
        categories = []
        for db_category in db_result:
            customer = Category(db_category[1], db_category[2])
            customer.id = db_category[0]
            categories.append(customer)
        return categories

    def get_category_max_id(self):
        try:
            db_result = self.repository.get_entity_max_id()
            return db_result
        except Exception as e:
            print(e, self.error_message)

    def get_customers_count(self):
        try:
            db_result = self.repository.get_entity_count()
            return db_result
        except Exception as e:
            print(e, self.error_message)
