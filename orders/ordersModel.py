from faker import Faker
from base.BaseModel import Base
fake = Faker()


class Order(Base):
    def __init__(self, customer_id, employee_id, order_date, shipper_id):
        self.orderID = 0
        self.customer_id = customer_id
        self.employee_id = employee_id
        self.order_date = order_date
        self.shipper_id = shipper_id


