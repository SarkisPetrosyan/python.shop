from base.BaseService import BaseService
from orders.ordersRepository import OrdersRep


class OrderService(BaseService):

    def __init__(self):
        super().__init__()
        self.repository = OrdersRep()

    def add_order(self, order):
        try:
            self.repository.add_order(order)
        except Exception as e:
            print(e, self.error_message)