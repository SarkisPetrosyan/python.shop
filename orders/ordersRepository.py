from base.BaseRepository import BaseRepository
from mysql.connector import Error


class OrdersRep(BaseRepository):
    def __init__(self):
        super().__init__()
        self.table = 'Orders'
        self.values = '(%s, %s, %s, %s)'
        self.create_table = '(id INT AUTO_INCREMENT PRIMARY KEY, customer_id INT, employee_id INT, ' \
                            'order_date VARCHAR(255), shipper_id INT,' \
                            'CONSTRAINT FK_CustomerOrder FOREIGN KEY (customer_id) REFERENCES customers(id)' \
                            'ON UPDATE CASCADE ON DELETE CASCADE,' \
                            'CONSTRAINT FK_EmployeeOrder FOREIGN KEY (employee_id) REFERENCES employees(id)' \
                            'ON UPDATE CASCADE ON DELETE CASCADE,' \
                            'CONSTRAINT FK_ShipperOrder FOREIGN KEY (shipper_id) REFERENCES shippers(id)' \
                            'ON UPDATE CASCADE ON DELETE CASCADE)'

    def add_order(self, order):
        try:
            if not self.db_connector.check_is_table_exists(self.table):
                sql = f"CREATE TABLE {self.table} VALUES {self.create_table}"
                self.db_connector.create_table(sql)

            self.add_entity(ord)
        except Error as e:
            print('Something went wrong... Error:', e)
