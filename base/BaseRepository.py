from dbconnector.dbConnector import DbConnector
from mysql.connector import Error


class BaseRepository:

    def __init__(self):
        self.db_connector = DbConnector()
        self.db = self.db_connector.db_connect()
        self.cursor = self.db.cursor()
        self.table = ""
        self.values = ""

    def edit_entity(self, entity, entity_id):
        try:
            text = f"UPDATE {self.table} SET "
            props = entity.get_props_tuple()
            values = entity.get_values()
            for (prop, value) in zip(props, values):
                text += f"{prop} = '{value}',"
            text = text[:-1]
            text += f" WHERE id = {entity_id}"
            self.cursor.execute(text)
            self.db.commit()
        except Error as e:
            print(e)

    def add_entity(self, entity):
        sql = f'INSERT INTO {self.table} {entity.get_props()} VALUES {self.values}'
        val = entity.get_values()
        self.cursor.execute(sql, val)
        self.db.commit()

    def get_entity_max_id(self):
        sql = f"Select max(id) from {self.table}"
        self.cursor.execute(sql)
        db_result = self.cursor.fetchone()
        return db_result[0]

    def get_entity_count(self):
        sql = f"Select count(id) from {self.table}"
        self.cursor.execute(sql)
        db_result = self.cursor.fetchone()
        return db_result[0]

    def get_entity(self, entity_id):
        sql = f"Select * from {self.table} where id = {entity_id}"
        self.cursor.execute(sql)
        db_result = self.cursor.fetchone()
        return db_result

    def delete_entity(self, entity_id):
        try:
            sql = f"Delete from {self.table} WHERE id = {entity_id}"
            self.cursor.execute(sql)
            self.db.commit()
        except Error as e:
            print(e)

    def get_all(self):
        sql = f"Select * from {self.table}"
        self.cursor.execute(sql)
        db_result = self.cursor.fetchall()
        return db_result
