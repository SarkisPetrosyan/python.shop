import inspect


class Base:

    def get_props(self):
        props = []
        for i in inspect.getmembers(self):
            if not i[0].startswith('_'):
                if not inspect.ismethod(i[1]) and i[0] != 'id':
                    props.append(i[0])
        return str(tuple(props)).replace('\'', '')

    def get_props_tuple(self):
        props = []
        for i in inspect.getmembers(self):
            if not i[0].startswith('_'):
                if not inspect.ismethod(i[1]) and i[0] != 'id':
                    props.append(i[0])
        return tuple(props)

    def get_values(self):
        values = []
        for i in inspect.getmembers(self):
            if not i[0].startswith('_'):
                if not inspect.ismethod(i[1]) and i[0] != 'id':
                    values.append(i[1])
        return tuple(values)
