from base.BaseRepository import BaseRepository


class BaseService:

    def __init__(self):
        self.repository = BaseRepository()
        self.error_message = "Something went wrong"

    def get_entity_max_id(self):
        try:
            db_result = self.repository.get_entity_max_id()
            return db_result
        except Exception as e:
            print(e, self.error_message)

    def get_count(self):
        try:
            db_result = self.repository.get_entity_count()
            return db_result
        except Exception as e:
            print(e, self.error_message)
